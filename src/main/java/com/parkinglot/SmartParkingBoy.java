package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhongShengYuan
 * @date 2023/7/15 15:17
 * @desc
 */
public class SmartParkingBoy extends ParkBoy{

    public SmartParkingBoy(int[] max) {
        super(max);
    }

    @Override
    public ParkingTicket park(Car car) {
        int index = 0;
        int mostFree = residuePositions(0);
        for (int i = 0; i < parkingLots.size(); i++) {
            if(mostFree < residuePositions(i)){
                index = i;
                mostFree = residuePositions(i);
            }
        }
        return parkingLots.get(index).park(car);

    }

    public int residuePositions(int i) {
        ParkingLot parkingLot = parkingLots.get(i);
        int residuePositions = capacity.get(i) - parkingLot.cars.size();
        return residuePositions;

    }
}
