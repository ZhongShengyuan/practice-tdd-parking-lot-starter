package com.parkinglot;

public class NotAvailablePositionException extends RuntimeException {

    public NotAvailablePositionException(String message) {
        super(message);
    }
}
