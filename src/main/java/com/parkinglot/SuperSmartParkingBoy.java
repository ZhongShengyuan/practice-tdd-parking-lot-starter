package com.parkinglot;

/**
 * @author ZhongShengYuan
 * @date 2023/7/15 16:48
 * @desc
 */
public class SuperSmartParkingBoy extends ParkBoy {

    public SuperSmartParkingBoy(int[] max) {
        super(max);
    }

    @Override
    public ParkingTicket park(Car car) {
        int index = 0;
        double mostFree = 0;
        for (int i = 0; i < parkingLots.size(); i++) {
            if (mostFree < residuePositions(i)) {
                index = i;
                mostFree = residuePositions(i);
            }
        }
        return parkingLots.get(index).park(car);
    }

    double residuePositions(int i) {
        ParkingLot parkingLot = parkingLots.get(i);
        double residuePositions = 1 - (parkingLot.cars.size() *1.0 / capacity.get(i)*1.0);
        return residuePositions;
    }
}
