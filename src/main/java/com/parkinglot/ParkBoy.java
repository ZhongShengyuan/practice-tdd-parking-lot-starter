package com.parkinglot;

import java.util.ArrayList;
import java.util.List;


public class ParkBoy {

    protected List<ParkingLot> parkingLots = new ArrayList<>();
    protected List<Integer> capacity = new ArrayList<>();

    public ParkBoy(int[] max) {
        for (int i : max) {
            parkingLots.add(new ParkingLot(i));
            capacity.add(i);
        }
    }

    public ParkingTicket park(Car car) {
        for (ParkingLot parkingLot : parkingLots) {
            if (!parkingLot.isFull()) {
                return parkingLot.park(car);
            }
        }
        return parkingLots.get(0).park(car);
    }

    public Car fetch(ParkingTicket ticket) {
        for (ParkingLot parkingLot : parkingLots) {
            if (!parkingLot.isFull()) {
                continue;
            }
            Car car = parkingLot.fetch(ticket);
            if (car != null) {
                return car;
            }
        }
        return parkingLots.get(0).fetch(ticket);
    }
}
