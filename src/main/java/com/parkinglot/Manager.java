package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhongShengYuan
 * @date 2023/7/15 20:28
 * @desc
 */
public class Manager extends ParkBoy {

    private List<ParkBoy> parkBoys = new ArrayList<>();

    public Manager(int[] max) {
        super(max);
    }

    public boolean addParkingBoy(ParkBoy parkBoy) {
        return parkBoys.add(parkBoy);
    }

    public ParkingTicket callParkingBoyPark(ParkBoy parkBoy, Car carA) {
        if (parkBoys.contains(parkBoy)) {
            ParkingTicket ticket = parkBoy.park(carA);
            return ticket;
        }
        throw new RuntimeException("NO SUCH RECOGNITION FOUND!");
    }

    public Car callParkingBoyFetch(ParkBoy parkBoy, ParkingTicket ticket) {
        if (parkBoys.contains(parkBoy)) {
            return parkBoy.fetch(ticket);
        }
        throw new RuntimeException("NO SUCH RECOGNITION FOUND!");
    }
}
