package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    protected Map<ParkingTicket, Car> cars = new HashMap();
    protected static int MAX;

    public ParkingLot(int max) {
        this.MAX = max;
    }

    public ParkingTicket park(Car car) {
        if (isFull()) {
            throw new NotAvailablePositionException("No available position.");
        }
        ParkingTicket ticket = new ParkingTicket();
        cars.put(ticket, car);
        return ticket;
    }

    public Car fetch(ParkingTicket ticket) {
        if (!cars.containsKey(ticket)) {
            throw new UnrecognizedParkingTicketException("Unrecognized parking tickt.");
        }
        Car car = cars.remove(ticket);
        return car;
    }

    public boolean isFull() {
        return cars.size() >= MAX;
    }
}
