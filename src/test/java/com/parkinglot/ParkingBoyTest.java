package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_parking_boy_and_car() {
        //given
        ParkBoy parkBoy = new ParkBoy(new int[]{10});
        Car car = new Car();

        //when
        ParkingTicket ticket = parkBoy.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_boy_and_ticket() {
        //given
        ParkBoy parkBoy = new ParkBoy(new int[]{10});
        Car car = new Car();
        ParkingTicket ticket = parkBoy.park(car);

        //when
        Car fetchCar = parkBoy.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_cars_when_fetch_two_car_given_parking_boy_and_tickets() {
        //given
        ParkBoy parkBoy = new ParkBoy(new int[]{10});
        Car carA = new Car();
        Car carB = new Car();
        ParkingTicket ticketA = parkBoy.park(carA);
        ParkingTicket ticketB = parkBoy.park(carB);

        //when
        Car fetchCarA = parkBoy.fetch(ticketA);
        Car fetchCarB = parkBoy.fetch(ticketB);

        //then
        assertEquals(carA, fetchCarA);
        assertEquals(carB, fetchCarB);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_boy_and_wrong_ticket() {
        ParkBoy parkBoy = new ParkBoy(new int[]{10});
        ParkingTicket wrongTicket = new ParkingTicket();

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_boy_and_used_ticket() {
        ParkBoy parkBoy = new ParkBoy(new int[]{10});
        Car car = new Car();
        ParkingTicket ticket = parkBoy.park(car);
        parkBoy.fetch(ticket);

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkBoy.fetch(ticket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_pakr_car_given_park_boy_and_full_parking_lot() {
        ParkBoy parkBoy = new ParkBoy(new int[]{2});
        Car carA = new Car();
        Car carB = new Car();
        Car carC = new Car();
        parkBoy.park(carA);
        parkBoy.park(carB);

        var ex = assertThrows(NotAvailablePositionException.class, () -> parkBoy.park(carC));
        assertEquals("No available position.", ex.getMessage());
    }
}
