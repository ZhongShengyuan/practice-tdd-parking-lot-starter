package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author ZhongShengYuan
 * @date 2023/7/15 15:10
 * @desc
 */
public class SmartParkingBoyTest {

    @Test
    void should_return_ticket_and_park_more_free_park_lot_when_park_given_smart_parking_boy_and_cars() {
        //given
        SmartParkingBoy smartPakringBoy = new SmartParkingBoy(new int[]{5, 5, 5});

        //when
        for (int i = 0; i < 5; i++) {
            smartPakringBoy.park(new Car());
        }

        //then
        int capacity0 = smartPakringBoy.residuePositions(0);
        int capacity1 = smartPakringBoy.residuePositions(1);
        int capacity2 = smartPakringBoy.residuePositions(2);
        assertEquals(true, capacity2 > capacity0 && capacity2 > capacity1);
    }

    @Test
    void should_return_tickets_when_two_park_given_smart_parking_boy_and_cars() {
        //given
        List<ParkingTicket> parkingTickets = new ArrayList<>();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new int[]{3, 3});

        //when
        for (int i = 0; i < smartParkingBoy.parkingLots.size(); i++) {
            parkingTickets.add(smartParkingBoy.park(new Car()));
        }

        //then
        for (int i = 0; i < smartParkingBoy.parkingLots.size(); i++) {
            assertNotNull(parkingTickets.get(i));
        }
    }

    @Test
    void should_return_car_when_fetch_given_smart_parking_boy_and_ticket() {
        //given
        SmartParkingBoy parkBoy = new SmartParkingBoy(new int[]{10});
        Car car = new Car();
        ParkingTicket ticket = parkBoy.park(car);

        //when
        Car fetchCar = parkBoy.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_cars_when_fetch_two_cars_given_smart_parking_boy_and_two_tickets() {
        //given
        SmartParkingBoy parkBoy = new SmartParkingBoy(new int[]{1, 1});
        Car carA = new Car();
        Car carB = new Car();
        ParkingTicket ticketA = parkBoy.park(carA);
        ParkingTicket ticketB = parkBoy.park(carB);

        //when
        Car fetchCarA = parkBoy.fetch(ticketA);
        Car fetchCarB = parkBoy.fetch(ticketB);

        //then
        assertEquals(carA, fetchCarA);
        assertEquals(carB, fetchCarB);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_smart_parking_boy_and_wrong_ticket() {
        SmartParkingBoy parkBoy = new SmartParkingBoy(new int[]{10});
        ParkingTicket wrongTicket = new ParkingTicket();

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_smart_parking_boy_and_used_ticket() {
        SmartParkingBoy parkBoy = new SmartParkingBoy(new int[]{10});
        Car car = new Car();
        ParkingTicket ticket = parkBoy.park(car);
        parkBoy.fetch(ticket);

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkBoy.fetch(ticket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_pakr_car_given_smart_park_boy_and_full_parking_lot() {
        SmartParkingBoy parkBoy = new SmartParkingBoy(new int[]{2});
        Car carA = new Car();
        Car carB = new Car();
        Car carC = new Car();
        parkBoy.park(carA);
        parkBoy.park(carB);

        var ex = assertThrows(NotAvailablePositionException.class, () -> parkBoy.park(carC));
        assertEquals("No available position.", ex.getMessage());
    }
}
