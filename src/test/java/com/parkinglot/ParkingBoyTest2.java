package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParkingBoyTest2 {

    @Test
    void should_return_ticket_when_two_park_given_parking_boy_and_cars() {
        //given
        int[] parkingLotsCapacity = new int[]{3, 3};
        List<ParkingTicket> parkingTickets = new ArrayList<>();
        ParkBoy parkBoy = new ParkBoy(parkingLotsCapacity);

        //when
        for (int i = 0; i < parkingLotsCapacity.length; i++) {
            parkingTickets.add(parkBoy.park(new Car()));
        }

        //then
        for (int i = 0; i < parkingLotsCapacity.length; i++) {
            assertNotNull(parkingTickets.get(i));
        }
    }

    @Test
    void should_return_ticket_when_park_given_parking_boy_and_a_full_park_lot_and_free_park_lot() {
        //given
        ParkBoy parkBoy = new ParkBoy(new int[]{1, 1});
        Car carA = new Car();
        Car carB = new Car();
        parkBoy.park(carA);

        //when
        parkBoy.park(carB);

        //then
        assertEquals(true, parkBoy.parkingLots.get(1).isFull());
    }

    @Test
    void should_return_cars_when_fetch_two_cars_given_parking_boy_and_two_tickets() {
        //given
        ParkBoy parkBoy = new ParkBoy(new int[]{1, 1});
        Car carA = new Car();
        Car carB = new Car();
        ParkingTicket ticketA = parkBoy.park(carA);
        ParkingTicket ticketB = parkBoy.park(carB);

        //when
        Car fetchCarA = parkBoy.fetch(ticketA);
        Car fetchCarB = parkBoy.fetch(ticketB);

        //then
        assertEquals(carA, fetchCarA);
        assertEquals(carB, fetchCarB);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_boy_and_and_two_park_lot_wrong_ticket() {
        ParkBoy parkBoy = new ParkBoy(new int[]{1, 1});
        ParkingTicket wrongTicket = new ParkingTicket();

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_boy_and_two_park_lot_used_ticket() {
        ParkBoy parkBoy = new ParkBoy(new int[]{1, 1});
        Car car = new Car();
        ParkingTicket ticket = parkBoy.park(car);
        parkBoy.fetch(ticket);

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkBoy.fetch(ticket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_pakr_car_given_park_boy_and_full_two_parking_lot() {
        ParkBoy parkBoy = new ParkBoy(new int[]{1, 1});
        Car carA = new Car();
        Car carB = new Car();
        Car carC = new Car();
        parkBoy.park(carA);
        parkBoy.park(carB);

        var ex = assertThrows(NotAvailablePositionException.class, () -> parkBoy.park(carC));
        assertEquals("No available position.", ex.getMessage());
    }
}
