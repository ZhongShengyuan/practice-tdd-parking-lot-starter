package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();

        //when
        ParkingTicket ticket = parkingLot.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);

        //when
        Car fetchCar = parkingLot.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_cars_when_fetch_two_car_given_parking_lot_and_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car carA = new Car();
        Car carB = new Car();
        ParkingTicket ticketA = parkingLot.park(carA);
        ParkingTicket ticketB = parkingLot.park(carB);

        //when
        Car fetchCarA = parkingLot.fetch(ticketA);
        Car fetchCarB = parkingLot.fetch(ticketB);

        //then
        assertEquals(carA, fetchCarA);
        assertEquals(carB, fetchCarB);
    }

/*    @Test
    void should_return_nothing_when_fetch_car_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket ticketA = parkingLot.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();

        //when
        Car fetchCarA = parkingLot.fetch(wrongTicket);

        //then
        assertEquals(null, fetchCarA);
    }
    @Test
    void should_return_nothing_when_fetch_car_given_parking_lot_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car carA = new Car();
        ParkingTicket ticketA = parkingLot.park(carA);

        //when
        Car fetchCarA = parkingLot.fetch(ticketA);
        Car usedTicket = parkingLot.fetch(ticketA);

        //then
        assertEquals(null, usedTicket);
    }

    @Test
    void should_return_nothing_when_fetch_car_given_parking_lot_and_without_position() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car carA = new Car();
        Car carB = new Car();
        Car carC = new Car();
        ParkingTicket ticketA = parkingLot.park(carA);
        ParkingTicket ticketB = parkingLot.park(carB);

        //when
        ParkingTicket ticketC = parkingLot.park(carC);

        //then
        assertEquals(null, ticketC);
    }*/

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_wrong_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingTicket wrongTicket = new ParkingTicket();

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_used_ticket() {
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(ticket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_pakr_car_given_full_parking_lot() {
        ParkingLot parkingLot = new ParkingLot(2);
        Car carA = new Car();
        Car carB = new Car();
        Car carC = new Car();
        parkingLot.park(carA);
        parkingLot.park(carB);

        var ex = assertThrows(NotAvailablePositionException.class, () -> parkingLot.park(carC));
        assertEquals("No available position.", ex.getMessage());
    }
}
