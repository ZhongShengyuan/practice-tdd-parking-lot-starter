package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author ZhongShengYuan
 * @date 2023/7/15 16:46
 * @desc
 */


public class SuperSmartParkingBoyTest {
    @Test
    void should_park_in_min_capositions_available_div_total_capacity_when_park_given_super_smart_parking_boy_and_cars() {
        //given
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(new int[]{3, 10});  //  2/3
        parkingBoy.parkingLots.get(0).park(new Car());
        parkingBoy.parkingLots.get(0).park(new Car());

        //when
        for (int i = 0; i < 6; i++) {
            parkingBoy.park(new Car());
        }

        //then
        assertEquals(6, parkingBoy.parkingLots.get(1).cars.size());
    }

    @Test
    void should_return_tickets_when_two_park_given_super_smart_parking_boy_and_cars() {
        //given
        List<ParkingTicket> parkingTickets = new ArrayList<>();
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(new int[]{3, 3});

        //when
        for (int i = 0; i < parkingBoy.parkingLots.size(); i++) {
            parkingTickets.add(parkingBoy.park(new Car()));
        }

        //then
        for (int i = 0; i < parkingBoy.parkingLots.size(); i++) {
            assertNotNull(parkingTickets.get(i));
        }
    }

    @Test
    void should_return_car_when_fetch_given_super_smart_parking_boy_and_ticket() {
        //given
        SuperSmartParkingBoy parkBoy = new SuperSmartParkingBoy(new int[]{10});
        Car car = new Car();
        ParkingTicket ticket = parkBoy.park(car);

        //when
        Car fetchCar = parkBoy.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_cars_when_fetch_two_cars_given_super_smart_parking_boy_and_two_tickets() {
        //given
        SuperSmartParkingBoy parkBoy = new SuperSmartParkingBoy(new int[]{1, 1});
        Car carA = new Car();
        Car carB = new Car();
        ParkingTicket ticketA = parkBoy.park(carA);
        ParkingTicket ticketB = parkBoy.park(carB);

        //when
        Car fetchCarA = parkBoy.fetch(ticketA);
        Car fetchCarB = parkBoy.fetch(ticketB);

        //then
        assertEquals(carA, fetchCarA);
        assertEquals(carB, fetchCarB);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_super_smart_parking_boy_and_wrong_ticket() {
        SuperSmartParkingBoy parkBoy = new SuperSmartParkingBoy(new int[]{10});
        ParkingTicket wrongTicket = new ParkingTicket();

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkBoy.fetch(wrongTicket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_super_smart_parking_boy_and_used_ticket() {
        SuperSmartParkingBoy parkBoy = new SuperSmartParkingBoy(new int[]{10});
        Car car = new Car();
        ParkingTicket ticket = parkBoy.park(car);
        parkBoy.fetch(ticket);

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> parkBoy.fetch(ticket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_pakr_car_given_super_smart_park_boy_and_full_parking_lot() {
        SuperSmartParkingBoy parkBoy = new SuperSmartParkingBoy(new int[]{2});
        Car carA = new Car();
        Car carB = new Car();
        Car carC = new Car();
        parkBoy.park(carA);
        parkBoy.park(carB);

        var ex = assertThrows(NotAvailablePositionException.class, () -> parkBoy.park(carC));
        assertEquals("No available position.", ex.getMessage());
    }
}
