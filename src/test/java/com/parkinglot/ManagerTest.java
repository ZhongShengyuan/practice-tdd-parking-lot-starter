package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author ZhongShengYuan
 * @date 2023/7/15 19:55
 * @desc
 */
public class ManagerTest {

    @Test
    void should_return_boolean_when_addParkingBoy_given_manage_and_parking_boys() {
        //given
        Manager manager = new Manager(new int[]{4});

        //when
        boolean add1 = manager.addParkingBoy(new ParkBoy(new int[]{10}));
        boolean add2 = manager.addParkingBoy(new SmartParkingBoy(new int[]{10}));
        boolean add3 = manager.addParkingBoy(new SuperSmartParkingBoy(new int[]{10}));

        //then
        assertEquals(true, add1 && add2 && add3);
    }

    @Test
    void should_return_ticket_when_callParkingBoyPark_given_manage_and_parking_boy_and_car() {
        //given
        Manager manager = new Manager(new int[]{4});
        ParkBoy parkingBoyA = new ParkBoy(new int[]{10});
        SmartParkingBoy parkingBoyB = new SmartParkingBoy(new int[]{10});
        SuperSmartParkingBoy parkingBoyC = new SuperSmartParkingBoy(new int[]{10});
        Car carA = new Car();
        Car carB = new Car();
        Car carC = new Car();
        manager.addParkingBoy(parkingBoyA);
        manager.addParkingBoy(parkingBoyB);
        manager.addParkingBoy(parkingBoyC);

        //when
        ParkingTicket ticketA = manager.callParkingBoyPark(parkingBoyA, carA);
        ParkingTicket ticketB = manager.callParkingBoyPark(parkingBoyB, carB);
        ParkingTicket ticketC = manager.callParkingBoyPark(parkingBoyC, carC);

        //then
        assertNotNull(ticketA);
        assertNotNull(ticketB);
        assertNotNull(ticketC);
    }

    @Test
    void should_return_Car_when_callParkingBoyFetch_given_manage_and_parking_boy_and_ticket() {
        //given
        Manager manager = new Manager(new int[]{4});
        ParkBoy parkingBoyA = new ParkBoy(new int[]{10});
        SmartParkingBoy parkingBoyB = new SmartParkingBoy(new int[]{10});
        SuperSmartParkingBoy parkingBoyC = new SuperSmartParkingBoy(new int[]{10});
        Car carA = new Car();
        Car carB = new Car();
        Car carC = new Car();
        manager.addParkingBoy(parkingBoyA);
        manager.addParkingBoy(parkingBoyB);
        manager.addParkingBoy(parkingBoyC);
        ParkingTicket ticketA = manager.callParkingBoyPark(parkingBoyA, carA);
        ParkingTicket ticketB = manager.callParkingBoyPark(parkingBoyB, carB);
        ParkingTicket ticketC = manager.callParkingBoyPark(parkingBoyC, carC);

        //when
        Car fetchCarA = manager.callParkingBoyFetch(parkingBoyA,ticketA);
        Car fetchCarB = manager.callParkingBoyFetch(parkingBoyB,ticketB);
        Car fetchCarC = manager.callParkingBoyFetch(parkingBoyC,ticketC);

        //then
        assertEquals(carA,fetchCarA);
        assertEquals(carB,fetchCarB);
        assertEquals(carC,fetchCarC);
    }

    @Test
    void should_return_ticket_when_park_given_manage_and_car() {
        //given
        Manager manager = new Manager(new int[]{4});
        Car car = new Car();

        //when
        ParkingTicket ticket = manager.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_manager_and_ticket() {
        //given
        Manager manager = new Manager(new int[]{10});
        Car car = new Car();
        ParkingTicket ticket = manager.park(car);

        //when
        Car fetchCar = manager.fetch(ticket);

        //then
        assertEquals(car, fetchCar);
    }

    @Test
    void should_return_cars_when_fetch_two_car_given_manager_and_tickets() {
        //given
        Manager manager = new Manager(new int[]{10});
        Car carA = new Car();
        Car carB = new Car();
        ParkingTicket ticketA = manager.park(carA);
        ParkingTicket ticketB = manager.park(carB);

        //when
        Car fetchCarA = manager.fetch(ticketA);
        Car fetchCarB = manager.fetch(ticketB);

        //then
        assertEquals(carA, fetchCarA);
        assertEquals(carB, fetchCarB);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_manager_and_wrong_ticket() {
        Manager manager = new Manager(new int[]{10});
        ParkingTicket wrongTicket = new ParkingTicket();

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> manager.fetch(wrongTicket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_manager_and_used_ticket() {
        Manager manager = new Manager(new int[]{10});
        Car car = new Car();
        ParkingTicket ticket = manager.park(car);
        manager.fetch(ticket);

        var ex = assertThrows(UnrecognizedParkingTicketException.class, () -> manager.fetch(ticket));
        assertEquals("Unrecognized parking tickt.", ex.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_pakr_car_given_manager_and_full_parking_lot() {
        Manager manager = new Manager(new int[]{2});
        Car carA = new Car();
        Car carB = new Car();
        Car carC = new Car();
        manager.park(carA);
        manager.park(carB);

        var ex = assertThrows(NotAvailablePositionException.class, () -> manager.park(carC));
        assertEquals("No available position.", ex.getMessage());
    }
}
